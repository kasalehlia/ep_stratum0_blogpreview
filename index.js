var eejs = require('ep_etherpad-lite/node/eejs/');

exports.eejsBlock_editbarMenuLeft = function (hook_name, args, cb) {
  args.content = args.content + eejs.require("ep_stratum0_blogpreview/templates/editbarButtons.ejs").replace('$$$padid$$$', args.renderContext.req.params.pad);
  return cb();
}

